package com.example.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;

@Controller
public class JavascriptController {

    @RequestMapping (value = "common.js", method = RequestMethod.GET)
	public String common(Model model, HttpServletResponse response) {
        return "common.js";
    }
}